from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *
from django.db.models import Q

def m(request):
    return render(request, 'pages/zavd2.html')

def reg(request):
    return render(request, 'pages/reg_for2.html')

def avt(request):
    return render(request, 'pages/avt_for2.html')

def mag(request):
    email = request.POST['email']
    password = request.POST['password']
    answers = {}
    if email and password:
        for i in User.objects.all():
            if email == i.email:
                answers['email_is_here'] = 'Такий email вже існує!'
                return render(request, 'pages/reg_for2.html', {'answers': answers})
    else:
        answers['none'] = 'Ви не заповнили всі поля!'
        return render(request, 'pages/reg_for2.html', {'answers': answers})
    User.objects.create(email=email, password=password)
    stat = Article.objects.all()
    hashtag = Hashtag.objects.all()
    return render(request, 'pages/my_mag.html', {'stat': stat, 'hashtag': hashtag})

def menu(request):
    stat = Article.objects.all()
    hashtag = Hashtag.objects.all()
    return render(request, 'pages/my_mag.html', {'stat': stat, 'hashtag': hashtag})

def new_avt(request):
    email = request.POST['email']
    password = request.POST['password']
    answers = {}
    if email and password:
        for i in User.objects.all():
            if email == i.email and password == i.password:
                hashtag = Hashtag.objects.all()
                stat = Article.objects.all()
                return render(request, 'pages/my_mag.html', {'stat': stat, 'hashtag': hashtag})
    else:
        answers['none'] = 'Ви не заповнили всі поля!'
        return render(request, 'pages/avt_for2.html', {'answers': answers})
    answers['email_is_here'] = 'Такого акаунта не існує!'
    return render(request, 'pages/avt_for2.html', {'answers': answers})

def your_hashtag(request, hashtag):
    a = Hashtag.objects.filter(pk=hashtag)
    yours = Article.objects.filter(hashtag=hashtag)
    return render(request, 'pages/hashtag.html', {'game': yours, 'c': a})

def your_art(request, name):
    coment = request.POST['coment']
    like = request.POST['like']
    yours = Article.objects.filter(pk=name)
    c = Comment.objects.filter(i=name)
    if coment:
        Comment.objects.create(i=name,com=coment)
    if like:
        Comment.objects.create(i=name,like=like)
    return render(request, 'pages/art.html', {'game': yours, 'c': c})

def poshuk(request):
    name = request.POST['q']
    if name:
        my_art = Article.objects.filter(Q(name__icontains=name))
        if my_art:
            return render(request, 'pages/your_art.html', {'art': my_art})
        else:
            my_art = Article.objects.filter(Q(name__startswith=name.title()))
            return render(request, 'pages/your_art.html', {'art': my_art})
    else:
        hashtag = Hashtag.objects.all()
        stat = Article.objects.all()
        return render(request, 'pages/my_mag.html', {'stat': stat, 'hashtag': hashtag})

def your_acaunt(request):
    return render(request, 'pages/about_me.html')

def about_you(request):
    user = request.POST['username']
    about = request.POST['about']
    if user and about:
        return render(request, 'pages/aboutyou.html', {'user': user, 'about': about})
    else:
        return render(request, 'pages/about_me.html')
