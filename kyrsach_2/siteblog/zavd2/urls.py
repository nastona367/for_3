from django.urls import path
from .views import *

urlpatterns = [
    path('', m, name='m'),
    path('reg/', reg, name='reg'),
    path('avt/', avt, name='avt'),
    path('mag/', mag, name='mag'),
    path('menu/', menu, name='menu'),
    path('posh/', poshuk, name='poshuk'),
    path('youracaut/', your_acaunt, name='your_acaunt'),
    path('aboutyou/', about_you, name='about_you'),
    path('avtready/', new_avt, name='new_avt'),
    path('<str:hashtag>/', your_hashtag, name='your_hashtag'),
    path('yourart/<int:name>/', your_art, name='your_art'),
    path('youracaut/', your_acaunt, name='your_acaunt')

]

