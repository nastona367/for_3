from django import forms
from django.contrib import admin
from .models import *

class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'password')

class CommentsAdmin(admin.ModelAdmin):
    list_display = ('i', 'pr', 'com')

class HashtagAdmin(admin.ModelAdmin):
    list_display = ('name',)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('hashtag', 'name', 'art', 'year', 'maker')

class AvatarAdmin(admin.ModelAdmin):
    list_display = ('email', 'username', 'year', 'about_me')
admin.site.register(User, UserAdmin)
admin.site.register(Hashtag, HashtagAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment, CommentsAdmin)
