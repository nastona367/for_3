from django.db import models


class Hashtag(models.Model):
    name = models.CharField(max_length=20, verbose_name="Назва хештегу", blank=True)

    def __str__(self):
        return self.name

class Article(models.Model):
    hashtag = models.ForeignKey('Hashtag', on_delete=models.CASCADE,
                                 null=True, verbose_name='Хештег')
    name = models.CharField(max_length=100, verbose_name="Назва статті", blank=True)
    art = models.CharField(max_length=5000, verbose_name="Стаття", blank=True)
    year = models.DateField(verbose_name="Дата написання", blank=True)
    maker = models.CharField(max_length=20, verbose_name="Автор", blank=True)

    def __str__(self):
        return self.name

class Comment(models.Model):
    i = models.IntegerField(default=1, blank=True)
    pr = models.ForeignKey('Article', on_delete=models.CASCADE,
                                 null=True, verbose_name='Стаття')
    com = models.CharField(max_length=100, verbose_name="Коментарій", blank=True)
    like = models.CharField(max_length=20, verbose_name="Лайк/дізлайк:", blank=True)

class User(models.Model):
    email = models.CharField(max_length=20, verbose_name="Пошта", blank=True)
    password = models.CharField(max_length=20, verbose_name="Пароль", blank=True)

