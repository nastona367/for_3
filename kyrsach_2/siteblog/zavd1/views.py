from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *
from django.db.models import Q

def menu(request):
    return render(request, 'pages/zavd1.html')

def reg(request):
    return render(request, 'pages/reg.html')

def avt(request):
    return render(request, 'pages/avt.html')

def mag(request):
    email = request.POST['email']
    password = request.POST['password']
    answers = {}
    if email and password:
        for i in User.objects.all():
            if email == i.email:
                answers['email_is_here'] = 'Такий email вже існує!'
                return render(request, 'pages/reg.html', {'answers': answers})
    else:
        answers['none'] = 'Ви не заповнили всі поля!'
        return render(request, 'pages/reg.html', {'answers': answers})
    User.objects.create(email=email, password=password)
    category = Category.objects.all()
    game = Product.objects.all()
    return render(request, 'pages/1.html', {'game': game, 'category': category})

def new_avt(request):
    email = request.POST['email']
    password = request.POST['password']
    answers = {}
    if email and password:
        for i in User.objects.all():
            if email == i.email and password == i.password:
                category = Category.objects.all()
                game = Product.objects.all()
                return render(request, 'pages/1.html', {'game': game, 'category': category})
    else:
        answers['none'] = 'Ви не заповнили всі поля!'
        return render(request, 'pages/avt.html', {'answers': answers})
    answers['email_is_here'] = 'Такого акаунта не існує!'
    return render(request, 'pages/avt.html', {'answers': answers})

def your_category(request, category):
    a = Category.objects.filter(pk=category)
    yours = Product.objects.filter(category=category)
    return render(request, 'pages/categorys.html', {'game': yours, 'c': a})

def your_product(request, product):
    coment = request.POST['coment']
    yours = Product.objects.filter(pk=product)
    if coment:
        Comment.objects.create(i=product,com=coment)
    c = Comment.objects.filter(i=product)
    return render(request, 'pages/product.html', {'game': yours, 'c': c})

def poshuk(request):
    name = request.POST['q']
    if name:
        my_product = Product.objects.filter(Q(product__icontains=name))
        if my_product:
            return render(request, 'pages/your_product.html', {'game': my_product})
        else:
            my_product = Product.objects.filter(Q(product__startswith=name.title()))
            return render(request, 'pages/your_product.html', {'game': my_product})
    else:
        category = Category.objects.all()
        game = Product.objects.all()
        return render(request, 'pages/1.html', {'game': game, 'category': category})

def buy(request, product):
    yours = Product.objects.filter(pk=product)
    return render(request, 'pages/buy.html', {'game': yours})

def good(request):
    poshta = request.POST['poshta']
    adres = request.POST['adres']
    a = {}
    if poshta and adres:
        return render(request, 'pages/good.html')
    else:
        a['none'] = ''
        yours = Product.objects.filter(pk=product)
        return render(request, 'pages/buy.html', {'game': yours,'a': a})
