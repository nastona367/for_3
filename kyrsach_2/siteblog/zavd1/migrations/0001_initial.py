# Generated by Django 4.0.6 on 2022-07-19 09:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=20, verbose_name='Назва категорії')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(blank=True, max_length=20, verbose_name='Пошта')),
                ('password', models.CharField(blank=True, max_length=20, verbose_name='Пароль')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product', models.CharField(blank=True, max_length=20, verbose_name='Товар')),
                ('price', models.IntegerField(blank=True, verbose_name='Ціна')),
                ('massa', models.IntegerField(blank=True, verbose_name='Масса (в гр)')),
                ('year', models.DateField(blank=True, verbose_name='Дата виготовлення')),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='zavd1.category', verbose_name='Категория')),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('com', models.CharField(blank=True, max_length=100, verbose_name='Коментарій')),
                ('pr', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='zavd1.product', verbose_name='Продукт')),
            ],
        ),
    ]
