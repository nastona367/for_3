from django.apps import AppConfig


class Zavd1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zavd1'
