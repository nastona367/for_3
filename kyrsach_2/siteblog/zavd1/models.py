from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name="Назва категорії", blank=True)

    def __str__(self):
        return self.name

class Product(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE,
                                 null=True, verbose_name='Категория')
    product = models.CharField(max_length=20, verbose_name="Товар", blank=True)
    price = models.IntegerField(verbose_name="Ціна", blank=True)
    massa = models.IntegerField(verbose_name="Масса (в гр)", blank=True)
    year = models.DateField(verbose_name="Дата виготовлення", blank=True)
    slug = models.SlugField(max_length=200, db_index=True, blank=True)

    def __str__(self):
        return self.product


class Comment(models.Model):
    i = models.IntegerField(default=1, blank=True)
    pr = models.ForeignKey('Product', on_delete=models.CASCADE,
                                 null=True, verbose_name='Продукт')
    com = models.CharField(max_length=100, verbose_name="Коментарій", blank=True)

class User(models.Model):
    email = models.CharField(max_length=20, verbose_name="Пошта", blank=True)
    password = models.CharField(max_length=20, verbose_name="Пароль", blank=True)
