from django.urls import path
from .views import *

urlpatterns = [
    path('', menu, name='menu'),
    path('reg/', reg, name='reg'),
    path('avt/', avt, name='avt'),
    path('mag/', mag, name='mag'),
    path('posh/', poshuk, name='poshuk'),
    path('buy/<int:product>/', buy, name='buy'),
    path('good/', good, name='good'),
    path('avtready/', new_avt, name='new_avt'),
    path('<str:category>/', your_category, name='your_category'),
    path('yourproduct/<int:product>/', your_product, name='your_product'),

]

